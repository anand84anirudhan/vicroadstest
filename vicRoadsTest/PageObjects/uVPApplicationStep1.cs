﻿using OpenQA.Selenium;
using System;
using OpenQA.Selenium.Support.UI;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using System.Threading;
using NUnit.Framework;

namespace vicRoadsTest.PageObjects
{
    class uVPApplicationStep1
    {
        private IWebDriver _Driver;
        public uVPApplicationStep1(IWebDriver driver) => _Driver = driver;
        ActionClass action = new();

        // Web elements in Step 1 ----------------------------

        IWebElement ddl_Vehicle_Type => _Driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_VehicleType_DDList"));
        IWebElement ddl_PassengerVehicleSubType => _Driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_PassengerVehicleSubType_DDList"));
        IWebElement ddl_GoodsVehicleSubType => _Driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_GoodsVehicleSubType_DDList"));
        IWebElement ddl_MotorcycleSubType => _Driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_MotorcycleSubType_DDList"));
        IWebElement txt_GarageAddress => _Driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_AddressLine_SingleLine_CtrlHolderDivShown"));
        IWebElement txt_PermitStartDate => _Driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitStartDate_Date"));
        IWebElement ddl_PermitDuration => _Driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitDuration_DDList"));
        IWebElement btn_Next => _Driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_btnNext"));
        IWebElement txt_progressbar => _Driver.FindElement(By.XPath("//div//p[contains(@class, 'progress-bar-title')]"));
        // ------------------------------------------------------



        public void navigateToApplicationPage()
        {
            _Driver.Navigate().GoToUrl("https://www.vicroads.vic.gov.au/registration/limited-use-permits/unregistered-vehicle-permits/get-an-unregistered-vehicle-permit");
            string progressTitle = txt_progressbar.Text;
            Assert.AreEqual("Step 1 of 7 : Calculate fee",
                            progressTitle);
        }

        public void enterVehicleDetails(Table table)
        {
            dynamic vehicleinfo = table.CreateDynamicInstance();
            switch (vehicleinfo.VehicleType)
            {
                case "Passenger vehicle":
                    action.SelectDropdown(ddl_Vehicle_Type, vehicleinfo.VehicleType);
                    action.SelectDropdown(ddl_PassengerVehicleSubType, vehicleinfo.Details);
                    action.EnterTextBox(txt_GarageAddress, vehicleinfo.Address);
                    action.EnterDateAsString(txt_PermitStartDate, vehicleinfo.PermitStartDate);
                    action.SelectDropdown(ddl_PermitDuration, vehicleinfo.PermitDuration);
                    Console.WriteLine("Passenger vehicle details entered");
                    break;

                case "Goods carrying vehicle":
                    action.SelectDropdown(ddl_Vehicle_Type, vehicleinfo.VehicleType);
                    action.SelectDropdown(ddl_GoodsVehicleSubType, vehicleinfo.Details);
                    action.EnterTextBox(txt_GarageAddress, vehicleinfo.Address);
                    action.EnterDateAsString(txt_PermitStartDate, vehicleinfo.PermitStartDate);
                    action.SelectDropdown(ddl_PermitDuration, vehicleinfo.PermitDuration);
                    Console.WriteLine("Goods vehicle detail Entered");
                    break;
                case "Prime Mover":
                    action.SelectDropdown(ddl_Vehicle_Type, vehicleinfo.VehicleType);
                    action.EnterTextBox(txt_GarageAddress, vehicleinfo.Address);
                    action.EnterDateAsString(txt_PermitStartDate, vehicleinfo.PermitStartDate);
                    action.SelectDropdown(ddl_PermitDuration, vehicleinfo.PermitDuration);
                    Console.WriteLine("Prime Mover detail Entered");
                    break;
                case "Motorcycle":
                    action.SelectDropdown(ddl_Vehicle_Type, vehicleinfo.VehicleType);
                    action.SelectDropdown(ddl_MotorcycleSubType, vehicleinfo.Details);
                    action.EnterTextBox(txt_GarageAddress, vehicleinfo.Address);
                    action.EnterDateAsString(txt_PermitStartDate, vehicleinfo.PermitStartDate);
                    action.SelectDropdown(ddl_PermitDuration, vehicleinfo.PermitDuration);
                    Console.WriteLine("Motorcycle detail Entered");
                    break;
                case "Trailer/Caravan":
                    action.SelectDropdown(ddl_Vehicle_Type, vehicleinfo.VehicleType);
                    action.EnterTextBox(txt_GarageAddress, vehicleinfo.Address);
                    action.EnterDateAsString(txt_PermitStartDate, vehicleinfo.PermitStartDate);
                    action.SelectDropdown(ddl_PermitDuration, vehicleinfo.PermitDuration);
                    Console.WriteLine("Motorcycle detail Entered");
                    break;


            }
        }

        public void ClickNext()
        {
            btn_Next.Click();
        }

    }
}
