﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vicRoadsTest.PageObjects
{
    class uVPApplicationStep2
    {
        private IWebDriver _Driver;
        public uVPApplicationStep2(IWebDriver driver) => _Driver = driver;
        ActionClass action = new();

        // Web elements in Step 1 ----------------------------
        IWebElement txt_progressbar => _Driver.FindElement(By.XPath("//div//p[contains(@class, 'progress-bar-title')]"));
        IWebElement Rdiobtn_PrepForRegistrationVic => _Driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitTypesRadio_RadioButtonList_0"));
        IWebElement Rdiobtn_SingleTrip => _Driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitTypesRadio_RadioButtonList_1"));
        IWebElement lbl_Permit1 => _Driver.FindElement(By.XPath("//label[text()='Preparation for Registration in Victoria']"));
        IWebElement lbl_Permit2 => _Driver.FindElement(By.XPath("//label[text()='Single Trip / Journey']"));

        //----------------------------------
        public void VerifyPage2()
        {
            string progressTitle = txt_progressbar.Text;
            Assert.AreEqual("Step 2 of 7 : Select permit type",
                            progressTitle);
            string permit1Text = lbl_Permit1.Text;
            string permit2Text = lbl_Permit2.Text;
            Assert.AreEqual("Preparation for Registration in Victoria",
                            permit1Text);
            Assert.AreEqual("Single Trip / Journey",
                            permit2Text);
           


        }
    }
}
