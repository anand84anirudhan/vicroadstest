﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;

namespace vicRoadsTest.Hooks
{
    [Binding]
    public class TestRunHooks
    {
        private DriverHelper _driverHelper;

        public TestRunHooks(DriverHelper driverHelper) => _driverHelper = driverHelper;

        //IWebDriver driver = new ChromeDriver();
        [BeforeScenario]
        public void BeforeScenario()
        {
            
            ChromeOptions option = new ChromeOptions();
            option.AddArgument("--disable-popup-blocking");
            new DriverManager().SetUpDriver(new ChromeConfig());
            _driverHelper.Driver = new ChromeDriver(option);
            _driverHelper.Driver.Manage().Cookies.DeleteAllCookies();
            _driverHelper.Driver.Manage().Window.Maximize();



        }

        [AfterScenario]
        public void AfterScenario()
        {
            _driverHelper.Driver.Quit();
        }
    }
}
