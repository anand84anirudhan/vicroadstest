﻿Feature: UVPTest
	Scenarios for unregistered vehicle permit application

@Test
Scenario: Verify that the permit type is displayed
	Given I Navigate to UVP application page
	And I enter the vehicle details
	| VehicleType       | Details | Address                             | PermitStartDate | PermitDuration |
	| Passenger vehicle | Sedan   | 70 wattle road, Hawthorn, Vic, 3122 | 03/01/2022      | 7 days         |
	When I click next
	Then Step 2 should display the permit type