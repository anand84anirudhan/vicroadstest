﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using vicRoadsTest.PageObjects;

namespace vicRoadsTest
{
    [Binding]
    public class UVPTestSteps
    {
        private DriverHelper _driverHelper;
        uVPApplicationStep1 uVPStep1;
        uVPApplicationStep2 uVPStep2;
        public UVPTestSteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            uVPStep1 = new uVPApplicationStep1(_driverHelper.Driver);
            uVPStep2 = new uVPApplicationStep2(_driverHelper.Driver);
        }
        


        [Given(@"I Navigate to UVP application page")]
        public void GivenINavigateToUVPApplicationPage()
        {
            uVPStep1.navigateToApplicationPage();
            Console.WriteLine("navigated to UVP application");
        }

        [Given(@"I enter the vehicle details")]
        public void GivenIEnterTheVehicleDetails(Table table)
        {
            uVPStep1.enterVehicleDetails(table);
            
        }

        [When(@"I click next")]
        public void WhenIClickNext()
        {
            uVPStep1.ClickNext();
            Console.WriteLine("clicked next");
        }

        [Then(@"Step(.*) should display the permit type")]
        public void ThenStepShouldDisplayThePermitType(int p0)
        {
            uVPStep2.VerifyPage2();
        }

    }
}
