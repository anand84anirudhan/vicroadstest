﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace vicRoadsTest
{
    class ActionClass
    {
        public void SelectDropdown(IWebElement element, string value)
        {
            SelectElement selectElement = new SelectElement(element);
            selectElement.SelectByText(value);
            Thread.Sleep(500);
        }

        public void EnterDateAsString(IWebElement element, DateTime value)
        {
            var shortdate = value.ToString("dd/MM/yyyy");
            element.Clear();
            element.SendKeys (shortdate);
            Thread.Sleep(500);
        }
        public void EnterTextBox(IWebElement element, string value)
        {
            element.SendKeys(value);
            Thread.Sleep(500);
        }
    }
}
